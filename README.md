# NFormat

**nformat** is JavaScript library that takes care of number
formatting and rounding.

**nformat** is a simple tool, it is not responsible for detecting/managing locale
settings or preventing user from inputing invalid values. It concentrates on
doing one job right.

## Why another module?

I needed simple tool for formatting values in input fields - one that would not
only make value look good, but also apply custom rounding in the meantime.
Since i didn't find any good module, i decided to write it myself.

## Basic usage

    nformat.format(1233, [options]);
    1 233.00

    nformat.parse('1233.00', [options]));
    1233

More docs to come..