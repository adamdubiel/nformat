/**
 * nformat - kiss number formatting and rounding.
 *
 * Copyright (c) 2013 Adam Dubiel <dubiel.adam@gmail.com>
 * Licensed under the MIT license.
 */

var nformat = (function() {

    var defaultDecimalSeparator = '.',
        defaultSignSymbol = '-';

    var defaultOptions = {
        decimalSeparator: '.',
        decimalNumbers: 2,
        signSymbol: '-',
        groupSize: 3,
        groupSeparator: ' ',
        prefix: '',
        suffix: '',
        round: 0.01,
        roundingMode: 'halfup'
    };

    var parseRoundFormat = function(value, options) {
        var number = value;
        if(typeof value === 'string') {
            number = parse(value, options);
        }

        var roundedNumber = round(number, options);
        return format(roundedNumber, options);
    };

    var round = function(currentValue, options) {
        var roundedValue;

        roundedValue = currentValue / options.round;
        if(options.roundingMode.toLowerCase() === 'floor') {
            roundedValue = Math.floor(roundedValue);
        }
        else if(options.roundingMode.toLowerCase() === 'ceiling') {
            roundedValue = Math.ceil(roundedValue);
        }
        else {
            roundedValue = Math.round(roundedValue);
        }
        roundedValue = roundedValue * options.round;

        return roundedValue;
    };

    var format = function(number, options) {
        var string = options.prefix, numberString, numberArray,
            charIndex, decimalSeparatorPosition;

        numberString = number.toFixed(options.decimalNumbers);
        numberArray = numberString.split('');
        if( number < 0 ) {
            numberArray.shift();
            string = string + options.signSymbol;
        }

        decimalSeparatorPosition = indexOf(numberArray, defaultDecimalSeparator);

        if( decimalSeparatorPosition > 0 ) {
            numberArray[decimalSeparatorPosition] = options.decimalSeparator;
        }

        if( options.groupSize > 0 ) {
            charIndex = decimalSeparatorPosition === -1 ? numberArray.length - 1 : decimalSeparatorPosition - 1;
            for(charIndex -= options.groupSize; charIndex >= 0; charIndex -= options.groupSize) {
                numberArray.splice(charIndex + 1, 0, options.groupSeparator);
            }
        }

        string = string + numberArray.join('');
        string = string + options.suffix;
        return string;
    };

    var indexOf = function(array, character) {
        if (!('indexOf' in Array.prototype)) {
            var i= 0;
            if (i<0) i+= array.length;
            if (i<0) i= 0;
            for (var n= array.length; i<n; i++)
                if (i in array && array[i]===character)
                    return i;
            return -1;
        } else {
            return array.indexOf(character);
        }
    };

    var extend = function(dest, source) {
        for (var prop in source) {
            if (!dest.hasOwnProperty(prop)) {
                dest[prop] = source[prop];
            }
        }
        return dest;
    };

    var parse = function(string, options) {
        var parsedString = string;

        parsedString = parsedString.replace(options.prefix, '').replace(options.sufix, '');
        parsedString = parsedString.replace(options.groupSeparator, '').replace(options.signSymbol, defaultSignSymbol);
        parsedString = parsedString.replace(options.decimalSeparator, defaultDecimalSeparator);

        return parseFloat(parsedString);
    };

    var mergeOptions = function(options) {
        var mergedOptions = {};
        extend(mergedOptions, options);
        extend(mergedOptions, defaultOptions);

        validateOptions(mergedOptions);
        return mergedOptions;
    };

    var validateOptions = function(options) {
        if( options.round === 0 ) {
            options.round = 1;
        }
    };

    var nformat = {};

    nformat.format = function(value, options) {
        var formatOptions = mergeOptions(options);
        return parseRoundFormat(value, formatOptions);
    };

    nformat.parse = function(value, options) {
        var parseOptions = mergeOptions(options);
        return parse(value, parseOptions);
    };

    nformat.defaults = function(options) {
        defaultOptions = mergeOptions(options);
    };

    return nformat;
}());
