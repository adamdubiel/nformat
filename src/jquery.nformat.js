/**
 * JQuery wrapper for nformat - Really Simple Number Formatter.
 *
 * Copyright (c) 2013 Adam Dubiel <dubiel.adam@gmail.com>
 * Licensed under the MIT license.
 */

(function($, nformat) {

    var namespace = 'nformat';

    var methods = {
        init: function(options) {
            return this.each(function() {
                var self = $(this),
                    data = self.data(namespace);

                if(options === undefined) {
                    options = {};
                }

                if(!data) {
                    self.data(namespace, options);
                    self.on('change.' + namespace, function() { self.nformat('format'); } );
                }
                else {
                    data = $.extend(true, data, options);
                    self.data(namespace, data);
                }

                self.nformat('format');
            });
        },

        format: function() {
            return this.each(function() {
                var self = $(this),
                    settings = self.data(namespace),
                    string;

                checkFieldSettings(settings);

                string = self.val();
                self.val(nformat.format(string, settings));
            });
        },

        setValue: function(value) {
            return this.each(function() {
                var self = $(this),
                    settings = self.data(namespace);

                checkFieldSettings(settings);

                self.val(nformat.format(value, settings));
            });
        },

        getValue: function() {
            var self = $(this[0]),
                settings = self.data(namespace);

            checkFieldSettings(settings);

            return nformat.parse(self.val(), settings);
        }
    };

    var checkFieldSettings = function(settings) {
        if(settings === undefined) {
            throw 'Trying to run nformat on uninitialized field!';
        }
    };

    $.fn.nformat = function(method) {
        if(methods[method]) {
            return methods[method].apply( this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' +  method + ' does not exist in rs-number-format');
        }
        return this;
    };

    $.nformat = {};

    $.nformat.format = function(value, options) {
        return nformat.format(value, options);
    };

    $.nformat.parse = function(value, options) {
        return nformat.parse(value, options);
    };

    $.nformat.defaults = function(options) {
        nformat.defaults(options);
    };

}(jQuery, nformat));