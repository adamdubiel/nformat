angular.module('nformat', []).directive('nformat', function factory() {
    return {
        restrict: 'AC',
        link: function(scope, element, attrs) {
            element.bind('change', function() {
                element.val(nformat.format(element.val(), attrs.nformat));
            });

            attrs.$observe('nformat', function(optionsJson) {
                if(optionsJson) {
                    var options = angular.fromJson(optionsJson);
                    element.val(nformat.format(element.val(), options));
                }
            });
        }
    };
});