module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            jshintrc: ".jshintrc",
            all: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js']
        },
        jasmine: {
            tests: {
                src: ['src/nformat.js', 'src/**/*.js'],
                options: {
                    vendor: ['bower_components/jquery/jquery.js', 'bower_components/angular/angular.js', 'bower_components/angular-mocks/angular-mocks.js'],
                    specs: 'test/**/*.js'
                }
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> - <%= pkg.version %> - ' +
                        '<%= pkg.author.name %> <<%= pkg.author.email %>> - ' +
                        '<%= pkg.license %> license - ' +
                        '<%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            always: {
                files: {
                    'dist/nformat.min.js' : ['src/nformat.js'],
                    'dist/jquery.nformat.min.js' : ['src/jquery.nformat.js'],
                    'dist/angular-nformat.min.js' : ['src/angular-nformat.js']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('default', ['jshint', 'jasmine', 'uglify']);
    grunt.registerTask('clean', ['clean']);
};
