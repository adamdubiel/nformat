describe('nformat core tests', function() {

    it('should format number using defaults', function() {
        var formatted = nformat.format(1234.32);
        expect(formatted).toEqual('1 234.32');
    });

    it('should format number with provided decimal separator', function() {
        var formatted = nformat.format(1234.32, {decimalSeparator: '*'});
        expect(formatted).toEqual('1 234*32');
    });

    it('should format negative number with provided sign symbol', function() {
        var formatted = nformat.format(-1234.32, {signSymbol: '#'});
        expect(formatted).toEqual('#1 234.32');
    });

    it('should format using provided group size', function() {
        var formatted = nformat.format(11234.32, {groupSize: 4});
        expect(formatted).toEqual('1 1234.32');
    });

    it('should format using provided group separator', function() {
        var formatted = nformat.format(1234.32, {groupSeparator: '$'});
        expect(formatted).toEqual('1$234.32');
    });

    it('should format and add specified prefix', function() {
        var formatted = nformat.format(1234.32, {prefix: 'PLN'});
        expect(formatted).toEqual('PLN1 234.32');
    });

    it('should format and add specified suffix', function() {
        var formatted = nformat.format(1234.32, {suffix: ' PLN'});
        expect(formatted).toEqual('1 234.32 PLN');
    });

    it('should format using rounding to 0.1 and halfup mode', function() {
        var formatted = nformat.format(1234.36, {round: 0.1, roundingMode: 'halfup'});
        expect(formatted).toEqual('1 234.40');
    });

    it('should format using rounding to 0.1 and ceiling mode', function() {
        var formatted = nformat.format(1234.31, {round: 0.1, roundingMode: 'ceiling'});
        expect(formatted).toEqual('1 234.40');
    });

    it('should format using rounding to 0.1 and floor mode', function() {
        var formatted = nformat.format(1234.39, {round: 0.1, roundingMode: 'floor'});
        expect(formatted).toEqual('1 234.30');
    });

    it('should format using rounding to 100 and floor mode', function() {
        var formatted = nformat.format(1234.39, {round: 100, roundingMode: 'floor'});
        expect(formatted).toEqual('1 200.00');
    });

    it('should format using rounding to 200 and ceiling mode', function() {
        var formatted = nformat.format(1234.39, {round: 200, roundingMode: 'ceiling'});
        expect(formatted).toEqual('1 400.00');
    });
});