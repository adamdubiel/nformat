describe('AngularJS directive test', function() {
    var elem, scope;

    beforeEach(module('nformat'));

    beforeEach(inject(function($rootScope, $compile) {
        elem = angular.element('<input nformat={{options}} type="text"/>');

        scope = $rootScope;
        $compile(elem)(scope);
        scope.$digest();
    }));

    it('should format value on change using defaults', function() {
        // given
        elem.val('1234');

        // when
        elem.change();

        // then
        expect(elem.val()).toEqual('1 234.00');
    });

    it('should format value on change using provided options', function() {
        // given
        elem.val('1234');

        // when
        scope.$apply(function() {
            scope.options = { decimalSeparator: ',', decimalNumbers: 1 };
        });

        // then
        expect(elem.val()).toEqual('1 234,0');
    });

});